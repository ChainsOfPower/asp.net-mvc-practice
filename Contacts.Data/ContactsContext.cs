﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Data
{
    using ent = Entities;

    public class ContactsContext : DbContext
    {
        public ContactsContext() : base(@"Data Source=IVAN-PC\SQLEXPRESS;Initial Catalog=dbNovaBP;Integrated Security=True")
        {
            Database.SetInitializer(new ContactsDatabaseInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ent::PersonConfiguration());
        }

        public IDbSet<ent::Person>      Persons         { get; set; }
        public IDbSet<ent::Contact>     Contacts        { get; set; }
    }
}
