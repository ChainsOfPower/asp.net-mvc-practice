﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Data
{
    using System.Data.Entity;
    using ent = Entities;

    public class ContactsDatabaseInitializer : DropCreateDatabaseAlways<ContactsContext>
    {
        public override void InitializeDatabase(ContactsContext context)
        {
            if(!context.Database.Exists())
            {
                context.Database.Create();
            }
            context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, $"ALTER DATABASE [{context.Database.Connection.Database}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
            base.InitializeDatabase(context);
        }

        protected override void Seed(ContactsContext context)
        {
            var random = new Random();
            var contacts = new[]
            {
                new ent::Contact() {Id = 1, Number = "0994020450" },
                new ent::Contact() {Id = 2, Number = "0912541777" },
                new ent::Contact() {Id = 3, Number = "0952222222" },
                new ent::Contact() {Id = 4, Number = "09522222232" },
                new ent::Contact() {Id = 5, Number = "09523222222" },
                new ent::Contact() {Id = 6, Number = "09522222222" },
                new ent::Contact() {Id = 7, Number = "09522221222" },
                new ent::Contact() {Id = 8, Number = "09522722222" },
                new ent::Contact() {Id = 9, Number = "0912222222" },
                new ent::Contact() {Id = 10, Number = "09526222222" },
            };

            Array.ForEach(new[]
            {
                new ent::Person() {Id = 1, FirstName = "Ivan", LastName = "Vukman", Contacts = contacts.Where(contact => contact.Id >= random.Next(0,10)).ToList() }
            }, _ => context.Persons.Add(_));

            context.SaveChanges();
        }
    }
}
