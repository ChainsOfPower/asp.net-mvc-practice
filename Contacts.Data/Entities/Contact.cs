﻿using System.ComponentModel.DataAnnotations;

namespace Contacts.Data.Entities
{
    public class Contact
    {
        public int                  Id                  { get; set; }

        [Required()]
        public string               Number              { get; set; }

        public virtual Person       Person              { get; set; }
    }
}