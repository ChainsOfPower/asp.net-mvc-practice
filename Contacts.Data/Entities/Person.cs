﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Data.Entities
{
    public class Person
    {
        public Person()
        {
            this.Contacts = new HashSet<Contact>();
        }

        public int                          Id              { get; set; }
        [Required()]
        public string                       FirstName       { get; set; }
        [Required()]
        public string                       LastName        { get; set; }
        public virtual ICollection<Contact> Contacts        { get; set; }
    }

    public class PersonConfiguration : EntityTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            HasMany(person => person.Contacts)
                .WithOptional(contact => contact.Person)
                .WillCascadeOnDelete();
        }
    }
}
