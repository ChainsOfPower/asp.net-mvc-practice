﻿using System.ComponentModel.DataAnnotations;

namespace Contacts.Services.Entities
{
    public class Contact
    {
        public int              Id              { get; set; }
        [Required()]
        public string           Number          { get; set; }
    }
}