﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Services.Entities
{
    public class Person
    {
        public Person()
        {
            this.Contacts = new List<Contact>();
        }

        public int                          Id                  { get; set; }
        [Required()]
        public string                       FirstName           { get; set; }
        [Required()]
        public string                       LastName            { get; set; }
        public IEnumerable<Contact>         Contacts            { get; set; }
    }
}
