﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;

namespace Contacts.Services
{
    using Data;

    using map = Entities;
    using ent = Data.Entities;

    public interface IContactsService
    {
        IEnumerable<map::Contact> GetAll();
        IEnumerable<map::Contact> GetAllNotUsed();
        void CreateContact(map::Contact contact);
        void DeleteContact(int id);
    }

    public class ContactsService : IContactsService
    {

        public ContactsService()
        {
            Context = new ContactsContext();
        }

        public void CreateContact(map.Contact contact)
        {
            var newContact = Mapper.Instance.Map<ent::Contact>(contact);

            newContact.Should().NotBeNull();

            Context.Contacts.Add(newContact);
            Context.SaveChanges();
        }

        public void DeleteContact(int id)
        {
            var contactToDelete = Context.Contacts.Find(id);
            if(contactToDelete != null)
            {
                Context.Contacts.Remove(contactToDelete);
                Context.SaveChanges();
            }
        }

        public IEnumerable<map.Contact> GetAll() => Context.Contacts.Select(Mapper.Map<map::Contact>);

        public IEnumerable<map.Contact> GetAllNotUsed() => Context.Contacts.Where(contact => contact.Person == null).Select(Mapper.Map<map::Contact>);

        private ContactsContext Context { get; }
    }
}
