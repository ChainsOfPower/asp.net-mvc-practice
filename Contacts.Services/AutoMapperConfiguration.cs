﻿using System.Linq;
using AutoMapper;

namespace Contacts.Services
{
    using map = Entities;
    using ent = Data.Entities;

    public static class AutoMapperConfiguration
    {
        public static void ConfigureMappings()
        {
            Mapper.Initialize((configuration =>
            {
                configuration.CreateMap<ent::Contact, map::Contact>().ReverseMap();

                configuration.CreateMap<ent::Person, map::Person>()
                .ConvertUsing<PersonConverter>();
            }

            ));
        }

        public class PersonConverter : ITypeConverter<ent::Person, map::Person>
        {
            public map::Person Convert(ResolutionContext context)
            {
                ent::Person person = (ent::Person)context.SourceValue;
                return new map.Person
                {
                    Id = person.Id,
                    FirstName = person.FirstName,
                    LastName = person.LastName,
                    Contacts = person.Contacts.Select(Mapper.Instance.Map<map::Contact>)
                };
            }
            
        }
    }
}
