﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using System.Data.Entity;

namespace Contacts.Services
{
    using Data;

    using map = Entities;
    using et = Data.Entities;
    using System.Data.Entity;
    public interface IPeopleService
    {
        IEnumerable<map::Person> GetAll();
        void CreatePerson(string firstName, string lastName, IEnumerable<int> selectedContacts);
        void DeletePerson(int id);
    }

    public class PeopleService : IPeopleService
    {
        public PeopleService()
        {
            Context = new ContactsContext();
        }

        public void CreatePerson(string firstName, string lastName, IEnumerable<int> selectedContacts)
        {
            var newPerson = Context.Persons.Create();

            newPerson.FirstName = firstName;
            newPerson.LastName = lastName;

            newPerson.Should().NotBeNull();

            if (selectedContacts != null)
            {
                foreach (var contactId in selectedContacts)
                {
                    newPerson.Contacts.Add(Context.Contacts.Find(contactId));
                }
            }

            Context.Persons.Add(newPerson);
            Context.SaveChanges();

        }

        public void DeletePerson(int id)
        {
            var personToDelete = Context.Persons.Find(id);
            if(personToDelete != null)
            {
                personToDelete.Contacts.Clear();

                Context.Persons.Remove(personToDelete);
                Context.SaveChanges();
            }
        }

        public IEnumerable<map.Person> GetAll() => Context.Persons
            .Include("Contacts")
            .Select(Mapper.Map<map::Person>);

        private ContactsContext Context { get; }
    }
}
