﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Contacts.Controllers
{
    using Services;
    using et = Data.Entities;
    using map = Services.Entities;

    public class ContactsController : Controller
    {
        public ContactsController()
        {
            _contactService = new ContactsService();
        }
        // GET: Contacts
        public ActionResult Index()
        {
            return View(model: _contactService.GetAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(et::Contact contact)
        {
            if(!ModelState.IsValid)
            {
                return View(contact);
            }
            _contactService.CreateContact(new map::Contact { Id = contact.Id, Number = contact.Number});
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            _contactService.DeleteContact(id);
            return RedirectToAction("Index");
        }

        private readonly IContactsService _contactService;
    }
}