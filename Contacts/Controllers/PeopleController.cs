﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Contacts.Services.Entities;
using Contacts.Services;
using System.ComponentModel.DataAnnotations;

namespace Contacts.Controllers
{
    public class PeopleController : Controller
    {
        public PeopleController()
        {
            _peopleService = new PeopleService();
            _contactsService = new ContactsService();
        }
        // GET: People
        public ActionResult Index()
        {
            return View(model: _peopleService.GetAll());
        }

        public ActionResult Create()
        {
            return View(model: new CreatePersonVm()
            {
                FirstName = "",
                LastName = "",
                PreloadedContacts = _contactsService.GetAllNotUsed()
            });
        }

        [HttpPost]
        public ActionResult Create(CreatePersonVm vm)
        {
            if (!ModelState.IsValid)
            {
                vm.PreloadedContacts = _contactsService.GetAllNotUsed();
                return View(vm);
            }

            _peopleService.CreatePerson(
                firstName: vm.FirstName,
                lastName: vm.LastName,
                selectedContacts: vm.SelectedContacts);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            _peopleService.DeletePerson(id);
            return RedirectToAction("Index");
        }

        private readonly IPeopleService _peopleService;
        private readonly IContactsService _contactsService;

     
    }

    public class CreatePersonVm
    {
        [Required()]
        public string               FirstName               { get; set; }
        [Required()]
        public string               LastName                { get; set; }

        public IEnumerable<Contact> PreloadedContacts       { get; set; }

        public IEnumerable<int> SelectedContacts            { get; set; }
    }
}